def transform(integers):        
    transformedList = list(integers)
    return transformedList

def getAverage(integers):
    min = integers[0]
    max = integers[0]
    for el in integers:
        if (el > max):
            max = el
        if (el < min):
            min = el
    
    integers.remove(min)
    integers.remove(max)

    sum = 0
    for el in integers:
        sum = sum + el

    return sum / len(integers) 

def main():
    integers = []
    for i in range(5):
        integers.append(int(input("Unesite broj: ")))
    print(int(getAverage(transform(integers))))
        
if __name__ == "__main__":
    main()
