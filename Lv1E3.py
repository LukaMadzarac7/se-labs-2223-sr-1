def isTwoNextToTwo(integers):
    for i in (range(len(integers) - 1)):
        if (integers[i] == 2):
            if (integers[i] == integers[i+1]):
                return True

    return False

def transform(integers):        
    transformedList = list(integers)
    return transformedList

def main():
    integers = []
    for i in range(5):
        integers.append(int(input("Unesite broj: ")))
    print(isTwoNextToTwo(integers))
        
if __name__ == "__main__":
    main()
