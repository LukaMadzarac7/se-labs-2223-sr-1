def char_types_count(string):
    upperCaseLettersCount = 0
    lowerCaseLettersCount = 0
    numberCount = 0
    for char in string:
        if (char.isupper()):
            upperCaseLettersCount = upperCaseLettersCount + 1
        if (char.islower()):
            lowerCaseLettersCount = lowerCaseLettersCount + 1
        if (char.isnumeric()):
            numberCount = numberCount + 1
    return [upperCaseLettersCount, lowerCaseLettersCount, numberCount]

def main():
    print(char_types_count(input("Unesite nesto: ")))

if __name__ == "__main__":
    main()
